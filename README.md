# SQLite Source Amalgamation

This repository contains the amalgamation of source code for the
[SQLite database engine](https://sqlite.org/).

Version: 3.31.1
